import React, { useContext } from "react";
import {Menu, Container, Button} from "semantic-ui-react";
import ProblemStore from "../../app/stores/problemStore"
import {observer} from "mobx-react-lite"

const NavBar: React.FC = () => {
  const problemStore = useContext(ProblemStore);
  const{openCreateForm} = problemStore;
  return (
    <Menu fixed="top" inverted>
      <Container>
        <Menu.Item>
          <img
            src="/assets/logo.png"
            alt="logo"
            style={{marginRight: "10px"}}
          />
          FAQ
        </Menu.Item>
        <Menu.Item name="Problems" />
        <Menu.Item>
          <Button onClick={openCreateForm} positive content="Create new Problem" />
        </Menu.Item>
      </Container>
    </Menu>
  );
};
export default observer(NavBar);
