import React, { useContext } from "react";
import {Card, Image, Button} from "semantic-ui-react";
import {observer} from "mobx-react-lite"
import ProblemStore from "../../../app/stores/problemStore";

const ProblemDetails: React.FC = () => {
  const problemStore = useContext(ProblemStore);
  const {selectedProblem:problem, openEditForm, cancelSelectedForm} = problemStore;
  return (
    <Card fluid>
      <Image src="/assets/screenshots/paul.png" wrapped ui={false} />
      <Card.Content>
        <Card.Header>{problem!.title}</Card.Header>
        <Card.Meta>
          <span className="date">{problem!.date}</span>
        </Card.Meta>
        <Card.Description>
          Description : {problem!.description} <br /> Solution :{" "}
          {problem!.solution}
        </Card.Description>
      </Card.Content>
      <Card.Content width={2}>
        <Button.Group widths={2}>
          <Button
            onClick={() => openEditForm(problem!.id)}
            basic
            color="blue"
            content="Edit"
          />
          <Button onClick={cancelSelectedForm} basic color="grey" content="Cancel"/>
        </Button.Group>
      </Card.Content>
    </Card>
  );
};

export default observer(ProblemDetails);
