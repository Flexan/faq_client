import React, {FormEvent, useContext, useState} from "react";
import {Segment, Form, Button} from "semantic-ui-react";
import {IProblem} from "../../../app/models/Problem";
import {v4 as uuid} from "uuid";
import {observer} from "mobx-react-lite";
import ProblemStore from "../../../app/stores/problemStore";

interface IProps {
  problem: IProblem;
}

const ProblemForm: React.FC<IProps> = ({
  problem: InitialFormState,
}) => {
  const problemStore = useContext(ProblemStore);
  const {createProblem,editProblem, submitting, cancelFormOpen} = problemStore;
  
  const getCurrentDate = (separator = "-") => {
    let newDate = new Date();
    let date = newDate.getDate();
    let month = newDate.getMonth() + 1;
    let year = newDate.getFullYear();

    return `${year}${separator}${
      month < 10 ? `0${month}` : `${month}`
    }${separator}${date}`;
  };
  const initializeForm = () => {
    if (InitialFormState) {
      return InitialFormState;
    } else {
      return {
        id: "",
        title: "",
        description: "",
        category: "",
        date: "",
        solution: "",
      };
    }
  };
  const [problem, setProblem] = useState<IProblem>(initializeForm);

  const handleSubmit = () => {
    if (problem.id.length === 0) {
      let newProblem = {...problem, id: uuid(), date: getCurrentDate()};
      createProblem(newProblem);
    } else {
      editProblem(problem);
    }
  };

  const handleInputChange = (
    event: FormEvent<HTMLInputElement | HTMLTextAreaElement>
  ) => {
    const {name, value} = event.currentTarget;
    setProblem({...problem, [name]: value});
  };
  return (
    <Segment clearing>
      <Form onSubmit={handleSubmit}>
        <Form.Input
          onChange={handleInputChange}
          name="title"
          placeholder="Title"
          value={problem.title}
        />
        <Form.TextArea
          onChange={handleInputChange}
          name="description"
          rows={2}
          placeholder="Description"
          value={problem.description}
        />
        <Form.Input
          onChange={handleInputChange}
          name="category"
          placeholder="Category"
          value={problem.category}
        />
        <Form.Input
          onChange={handleInputChange}
          name="date"
          type="date"
          placeholder="Date"
          value={problem.date}
        />
        <Form.TextArea
          onChange={handleInputChange}
          name="solution"
          placeholder="Solution"
          value={problem.solution}
        />
        <Button
          loading={submitting}
          floated="right"
          positive
          type="submit"
          content="Submit"
        />
        <Button
          onClick={cancelFormOpen}
          floated="right"
          type="button"
          content="Cancel"
        />
      </Form>
    </Segment>
  );
};
export default observer(ProblemForm);
