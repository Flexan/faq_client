import React, {useContext} from "react";
import {Item, Button, Label, Segment} from "semantic-ui-react";
import ProblemStore from "../../../app/stores/problemStore";
import {observer} from "mobx-react-lite";

const ProblemList: React.FC = () => {
  const problemStore = useContext(ProblemStore);
  const {problemsByDate, selectProblem} = problemStore;
  return (
    <Segment clearing>
      <Item.Group divided>
        {problemsByDate.map((problem) => (
          <Item key={problem.id}>
            <Item.Content>
              <Item.Header>{problem.title}</Item.Header>
              <Item.Meta>{problem.date}</Item.Meta>
              <Item.Description>
                <div>{problem.description}</div>
                <div>{problem.solution}</div>
              </Item.Description>
              <Item.Extra>
                <Button
                  onClick={() => selectProblem(problem.id)}
                  floated="right"
                  content="view"
                  color="blue"
                />
                <Label basic content={problem.category} />
              </Item.Extra>
            </Item.Content>
          </Item>
        ))}
      </Item.Group>
    </Segment>
  );
};
export default observer(ProblemList);
