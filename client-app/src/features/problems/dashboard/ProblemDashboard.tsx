import React, {useContext} from "react";
import {Grid} from "semantic-ui-react";
import ProblemList from "./ProblemList";
import ProblemDetails from "../details/ProblemDetails";
import ProblemForm from "../form/ProblemForm";
import {observer} from "mobx-react-lite";
import ProblemStore from "../../../app/stores/problemStore";


const ProblemDashboard: React.FC = ({
}) => {
  const problemStore = useContext(ProblemStore);
  const {editMode, selectedProblem} = problemStore;

  return (
    <Grid>
      <Grid.Column width={10}>
        <ProblemList />
      </Grid.Column>
      <Grid.Column width={6}>
        {selectedProblem && !editMode && (<ProblemDetails />)}
        {editMode && (
          <ProblemForm
            key={(selectedProblem && selectedProblem.id) || 0}
            problem={selectedProblem!}
          />
        )}
      </Grid.Column>
    </Grid>
  );
};

export default observer(ProblemDashboard);
