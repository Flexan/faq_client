export interface IProblem{
    id : string;
    title : string;
    description : string;
    category : string;
    date: string;
    solution : string;
}