import { IProblem } from './../models/Problem';
import axios, { AxiosResponse } from 'axios';


axios.defaults.baseURL = 'http://localhost:5000/api'; //apiurl

const responseBody = (response: AxiosResponse) => response.data;

const sleep = (ms : number) => (response : AxiosResponse) =>
    new Promise<AxiosResponse>(resolve => setTimeout(() => resolve(response), ms));

const request = {
    get: (url: string) => axios.get(url).then(sleep(1000)).then(responseBody),
    post: (url: string, body: {}) => axios.post(url, body).then(sleep(1000)).then(responseBody),
    put: (url: string, body: {}) => axios.put(url, body).then(sleep(1000)).then(responseBody),
    del: (url: string) => axios.delete(url).then(sleep(1000)).then(responseBody)
}

const Problems ={
    list: () :Promise<IProblem[]> => request.get('/problems'),
    details: (id : string) => request.get(`/problems/${id}`),
    create: (problem : IProblem) => request.post('/problems/',problem),
    update: (problem : IProblem) => request.put(`/problems/${problem.id}`, problem)
}

export default { 
    Problems
} 