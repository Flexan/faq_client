import { IProblem } from './../models/Problem';
import { observable, action, computed, configure, runInAction } from 'mobx';
import { createContext } from 'react';
import agent from '../api/agent';

configure({ enforceActions: 'always' });

class ProblemStore {
    @observable problemRegistry = new Map();
    @observable problems: IProblem[] = [];
    @observable loadingInitial = false;
    @observable selectedProblem: IProblem | undefined;
    @observable editMode = false;
    @observable submitting = false;

    @computed get problemsByDate() {
        return Array.from(this.problemRegistry.values()).sort((a, b) => Date.parse(b.date) - Date.parse(a.date))
    }

    @action loadProblems = async () => {
        this.loadingInitial = true;
        try {
            const problems = await agent.Problems.list();
            runInAction('loading problems',() => {
                problems.forEach((problem) => {
                    problem.date = problem.date.split("T")[0];
                    this.problemRegistry.set(problem.id, problem);
                });
                this.loadingInitial = false
            })

        } catch (error) {
            console.log(error);
            runInAction('loading problems error',() => {
                this.loadingInitial = false;
            })
        }

    }

    @action createProblem = async (problem: IProblem) => {
        this.submitting = true
        try {
            await agent.Problems.create(problem);
            runInAction('creating problems',()=>{
                this.problemRegistry.set(problem.id, problem);
                this.submitting = false;
                this.editMode = false;
            })
        } catch (error) {
            console.log(error);
            runInAction('creating problems error',() => {
                this.submitting = false;
            })
            
        }

    }

    @action editProblem = async (problem: IProblem) => {
        this.submitting = true
        try {
            await agent.Problems.update(problem);
            runInAction('editing problems',() => {
                this.problemRegistry.set(problem.id, problem);
                this.selectedProblem = problem;
                this.editMode = false;
                this.submitting = false;
            })
        } catch (error) {
            console.log(error);
            runInAction('editing problems',() => {
                this.submitting = false;
            })
        }
    }

    @action openCreateForm = () => {
        this.editMode = true;
        this.selectedProblem = undefined;
    }

    @action openEditForm = (id: string) => {
        this.selectedProblem = this.problemRegistry.get(id);
        this.editMode = true;
    }

    @action cancelSelectedForm = () => {
        this.selectedProblem = undefined;
    }
    @action cancelFormOpen = () => {
        this.editMode = false;
    }

    @action selectProblem = (id: string) => {
        this.selectedProblem = this.problemRegistry.get(id);
        this.editMode = false;
    }
}

export default createContext(new ProblemStore())