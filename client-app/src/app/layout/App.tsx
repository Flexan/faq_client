import React, { useEffect, Fragment, useContext} from "react";
import {Container} from "semantic-ui-react";
import NavBar from "../../features/nav/NavBar";
import ProblemDashboard from "../../features/problems/dashboard/ProblemDashboard";
import {LoadingComponent} from "./LoadingComponent";
import ProblemStore from "../../app/stores/problemStore";
import {observer} from "mobx-react-lite";
// interface IState {
//   problems : IProblem[]
// }

const App = () => {
  // readonly state : IState = {
  //   problems: [],
  // };

  // componentDidMount() {
  //   axios.get<IProblem[]>("http://localhost:5000/api/problems").then((response) => {
  //     this.setState({
  //       problems: response.data,
  //     });
  //   });
  // }

  const problemStore = useContext(ProblemStore);

  useEffect(() => {
    problemStore.loadProblems();
  }, [problemStore]);

  if (problemStore.loadingInitial)
    return <LoadingComponent content="Please wait...." />;

  // render() {
  return (
    <Fragment>
      <NavBar />
      <Container style={{marginTop: "7em"}}>
        <ProblemDashboard/>
      </Container>
    </Fragment>
  );
  //  } //render close bracket
};

export default observer(App);
